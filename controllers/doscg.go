package controllers

import (
	"encoding/json"
	"fmt"

	"doscg/models"

	"github.com/astaxie/beego"
)

// DoSCGController ...
type DoSCGController struct {
	beego.Controller
}

// PostSeries ...
func (cntlr *DoSCGController) PostSeries() {

	req := models.Series{}
	body := cntlr.Ctx.Input.RequestBody
	if err := json.Unmarshal(body, &req); err != nil {
		cntlr.CustomAbort(400, "Bad Request")
	}

	res := arithmeticSeries(findNumN, findNumNMinusOne, req.InputSlice)

	cntlr.Data["json"] = res
	cntlr.ServeJSON()
}

// GetFindBC ...
func (cntlr *DoSCGController) GetFindBC() {
	b, c := findBC(21)
	res := models.BCRes{
		B: b,
		C: c,
	}
	cntlr.Data["json"] = res
	cntlr.ServeJSON()
}

// PostFindRoute ...
func (cntlr *DoSCGController) PostFindRoute() {
	req := models.Route{}
	body := cntlr.Ctx.Input.RequestBody
	if err := json.Unmarshal(body, &req); err != nil {
		cntlr.CustomAbort(400, "Bad Request")
	}
	baseurl := beego.AppConfig.String("mapbaseurl")
	apikey := beego.AppConfig.String("mapapikey")

	url := fmt.Sprintf("%s?key=%s&origin=%s&destination=%s&avoid=tolls|highways", baseurl, apikey, req.Origin, req.Destination)
	res := models.RouteRes{
		URL: url,
	}
	cntlr.Data["json"] = res
	cntlr.ServeJSON()
}

func arithmeticSeries(fn func(n, numNMinusOne int) int,
	fnMinus1 func(n, numNMinusOne int) int, list []*int) []int {
	i := 0
	p := 0
	length := len(list)
	res := make([]int, len(list))

	for i < length {
		if list[i] == nil {
			i++
			continue
		} else {
			res[i] = toInt(list[i])
		}

		if i > 0 && list[i-1] == nil {
			val := fnMinus1(i, toInt(list[i]))
			list[i-1] = toPointer(val)
			res[i-1] = val
			if p < i {
				p = i
			}
			i--
		}
		if i+1 < length && list[i+1] == nil {
			val := fn(i+1, toInt(list[i]))
			list[i+1] = toPointer(val)
			res[i+1] = val
			i++
			p = i
		}
		if i == 0 {
			if p > 0 {
				i = p
			} else {
				i++
			}
			continue
		}
		if (i > 0 && i < length-1) && list[i-1] != nil && list[i+1] != nil {
			res[i-1] = toInt(list[i-1])
			res[i+1] = toInt(list[i+1])
			i++
		}
		if i == length-1 && list[i] != nil {
			res[i] = toInt(list[i])
			i++
		}

	}
	return res
}

func toPointer(n int) *int {
	val := &n
	return val
}

func toInt(n *int) int {
	val := *n
	return val
}

func findNumNMinusOne(n, numN int) int {
	return numN - (n-1)*2
}

func findNumN(n, numNMinusOne int) int {
	return numNMinusOne + ((n - 1) * 2)
}

func findBC(a int) (b, c int) {
	b = 23 - a
	c = -21 - a
	return
}
