package controllers

import (
	"doscg/models"
	"net/http"
	"net/url"
	"strings"

	"github.com/astaxie/beego"
)

type LineController struct {
	beego.Controller
}

func (cntlr *LineController) PostNotify() {
	lineendpoint := beego.AppConfig.String("linenotifyendpoint")
	linenotifytoken := beego.AppConfig.String("linenotifytoken")
	authtoken := beego.AppConfig.String("authtoken")
	token := cntlr.GetString("auth_token")
	if authtoken != token {
		cntlr.CustomAbort(401, "Unauthorized")
	}
	form := url.Values{
		"message": {"Line Bot can not answer a question to the customer more than 10 second"}}

	client := &http.Client{}
	req, err := http.NewRequest("POST", lineendpoint, strings.NewReader(form.Encode()))
	if err != nil {
		cntlr.CustomAbort(400, "Bad Request")
	}
	bearer := "Bearer " + linenotifytoken
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Authorization", bearer)
	res, err := client.Do(req)
	if err != nil {
		cntlr.CustomAbort(400, err.Error())
	}
	if res.StatusCode != 200 {
		cntlr.CustomAbort(res.StatusCode, "Error")
	}

	json := models.LineRes{
		Msg: "Message sent",
	}
	cntlr.Data["json"] = json
	cntlr.ServeJSON()
}
