package main

import (
	_ "doscg/routers"
	"fmt"
	"log"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/config"
	"github.com/astaxie/beego/plugins/cors"
)

func init() {

	iniconf, err := config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		log.Fatal(iniconf)
	}
	fmt.Println(iniconf.String("copyrequestbody"))
}

func main() {
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST", "DELETE", "PUT", "PATCH"},
		AllowHeaders:     []string{"Origin", "content-type", "Access-Control-Allow-Origin"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))
	beego.Run()
}
