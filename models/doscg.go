package models

// Series ...
type Series struct {
	InputSlice []*int `json:"inputSlice"`
}

// BCRes ...
type BCRes struct {
	B int `json:"B"`
	C int `json:"C"`
}

// Route ...
type Route struct {
	Origin      string `json:"origin"`
	Destination string `json:"destination"`
}

// RouteRes ...
type RouteRes struct {
	URL string `json:"url"`
}

type LineRes struct {
	Msg string `json:"msg"`
}
