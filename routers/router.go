package routers

import (
	"doscg/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})

	beego.Router("/doscg/findbc", &controllers.DoSCGController{}, "get:GetFindBC")
	beego.Router("/doscg/findxyz", &controllers.DoSCGController{}, "post:PostSeries")
	beego.Router("/doscg/findroute", &controllers.DoSCGController{}, "post:PostFindRoute")
	beego.Router("/doscg/linenoti", &controllers.LineController{}, "post:PostNotify")
}
